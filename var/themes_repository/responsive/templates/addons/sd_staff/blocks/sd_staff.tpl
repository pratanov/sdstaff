{script src="js/addons/sd_staff/func.js"}
{if $items}
    <div class="team-slide owl-carousel">
        <ul id="team-slider">
	           {foreach from=$items item="member" key="key"}
	               <li class="item" style="margin:0px; max-width:100px !important;">
                    {if $member.main_pair}
        	               <img src="images/{$member.main_pair.icon.relative_path}" alt="{$member.name} {$member.last_name}" style="max-height:100px;" /> 
	                   {else}
                        <img src="images/no_image.png" style="max-height:100px;" alt="{$member.name} {$member.last_name}" />                        
                    {/if}
                    <h4>{$member.name} {$member.last_name}</h4>
					               {$member.function}<br />
                    <div id="sd_staff_member_item_{$member.staff_id}">
                        {if $member_mail}
                            <a class="sd_staff_mailto" href="mailto:{$member_mail}">{$member_mail}</a>                        
                        {else}
                            <a class="sd_staff_view_mail" data-member-id="{$member.staff_id}">Показать e-mail</a>
                        {/if}                        
                    <!--sd_staff_member_item_{$member.staff_id}--></div>
	               </li>
	          {/foreach}
       </ul>
    </div>
{/if}