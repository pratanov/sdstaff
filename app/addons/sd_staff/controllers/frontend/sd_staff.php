<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode =='sd_staff_get_member_mail') {
    $params = $_POST;

    if (isset($params['staff_id'])) {
        $member_mail = fn_sd_staff_get_member_mail($params['staff_id']);
    	Tygh::$app['view']->assign('member_mail', $member_mail);
    	Tygh::$app['view']->display('addons/sd_staff/blocks/sd_staff.tpl');
    }
}
