<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD']  == 'POST') {
    fn_trusted_vars('members', 'member_data', 'member', 'result');
    $suffix = '';

    if ($mode == 'edit_item') {
        $member_data = isset($_REQUEST['member_data']) ? $_REQUEST['member_data'] : null;
        $member = (isset($member_data['staff_id']) && ($member_data['staff_id'] > 0)) ? fn_sd_staff_update_member($member_data) : fn_sd_staff_add_member($member_data);
        $member_id = $member['staff_id'];

        $suffix = ".edit_item?staff_id=$member_id";
    }

    if ($mode == 'delete') {
        $delete_result = (isset($_REQUEST['staff_id']) && ($_REQUEST['staff_id'] > 0)) ? fn_sd_staff_delete_member($_REQUEST['staff_id']) : "not_found";

        $result = isset($delete_result) ? "success" : "not_found";
        $suffix = '.manage';
    }

    return array(CONTROLLER_STATUS_OK, 'sd_staff' . $suffix);
}

if ($mode == 'manage') {
    $members = fn_sd_staff_get_members();

    Tygh::$app['view']->assign('members', $members);
} elseif ($mode == 'edit_item') {
    if (isset($_REQUEST['staff_id'])) {
        $staff_id = $_REQUEST['staff_id'];
        $member = fn_sd_staff_get_member($staff_id);

        if (isset($member)) {
            $member['main_pair'] = fn_get_image_pairs($member['staff_id'], 'staff', 'M', '?:sd_staff');
        } else {
            Tygh::$app['view']->assign('error', 'not_found');
        }
    } else {
        $member['sort'] = 0;
        $member['user_id'] = 0;
    }


    if (isset($member)) {
        Tygh::$app['view']->assign('member', $member);
    }
}
