<?php

$schema['sd_staff'] = array(
    'templates' => array(
        'addons/sd_staff/blocks/sd_staff.tpl' => array(),
    ),
    'wrappers' => 'blocks/wrappers',
    'content' => array(
        'items' => array(
            'remove_indent' => true,
            'hide_label' => true,
            'type' => 'enum',
            'object' => 'members',
            'items_function' => 'fn_get_members',
        ),
    ),
    'cache' => array(
        'update_handlers' => array('sd_staff'),
    ),
);

return $schema;
